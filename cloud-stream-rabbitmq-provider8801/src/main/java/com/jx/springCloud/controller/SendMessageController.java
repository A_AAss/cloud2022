package com.jx.springCloud.controller;

import com.jx.springCloud.service.MessageProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LDW
 * @date 2022/4/10 21:20
 */
@RestController
public class SendMessageController {

    private final MessageProvider messageProvider;

    public SendMessageController(MessageProvider messageProvider) {
        this.messageProvider = messageProvider;
    }

    @GetMapping("/sendMessage")
    public String sendMessage() {
        return messageProvider.send();
    }
}
