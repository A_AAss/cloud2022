package com.jx.springCloud.service;

/**
 * @author LDW
 * @date 2022/4/10 20:18
 */
public interface MessageProvider {
    String send();
}
