package com.jx.springCloud.service.impl;

import com.jx.springCloud.service.MessageProvider;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author LDW
 * @date 2022/4/10 20:35
 */
@Service
public class MessageProviderImpl implements MessageProvider {

    private final StreamBridge streamBridge;

    public MessageProviderImpl(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        streamBridge.send("myChannel-out-0", MessageBuilder.withPayload(serial).build());
        System.out.println("发送消息: " + serial);
        return null;
    }
}
