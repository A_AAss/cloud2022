package com.jx.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author LDW
 * @date 2022/4/9 11:43
 */
/*
    如果想要实时监控端口
    1.引入spring-boot-starter-actuator依赖
    2.在yml文件中添加暴露监控端点的配置
    3.在controller类上添加@RefreshScope注解
    4.当重新修改远程仓库上的配置文件后，需要发送一个post请求：http://localhost:3355/actuator/refresh 来刷新获取配置
        请求体类型应该是json类型
      或者在控制台发送命令：curl -X POST "http://localhost:3355/actuator/refresh"
    以上属于是手动版的动态刷新
 */
@SpringBootApplication
@EnableEurekaClient
public class CloudConfigClientMain3355 {
    public static void main(String[] args) {
        SpringApplication.run(CloudConfigClientMain3355.class, args);
    }
}
