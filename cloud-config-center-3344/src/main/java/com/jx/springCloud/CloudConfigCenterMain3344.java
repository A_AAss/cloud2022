package com.jx.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author LDW
 * @date 2022/4/9 10:32
 */
@SpringBootApplication
@EnableConfigServer
public class CloudConfigCenterMain3344 {
    public static void main(String[] args) {
        SpringApplication.run(CloudConfigCenterMain3344.class, args);
    }
}
