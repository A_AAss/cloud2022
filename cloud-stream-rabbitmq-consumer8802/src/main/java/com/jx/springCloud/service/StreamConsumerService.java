package com.jx.springCloud.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

/**
 * @author LDW
 * @date 2022/4/11 12:00
 */
@Service
@Slf4j
public class StreamConsumerService {
    @Bean
    public Consumer<String> myChannel() {
        return message -> log.info("consumer8802->消息："+message);
    }
}
