package com.jx.springCloud.myHandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.jx.springCloud.entities.CommonResult;

/**
 * @author LDW
 * @date 2022/4/15 10:49
 */
public class CustomerBlockHandler {

    public static CommonResult handlerException(BlockException blockException) {
        return new CommonResult(444, "客户自定义1,global handlerException");
    }

    public static CommonResult handlerException2(BlockException blockException) {
        return new CommonResult(444, "客户自定义2,global handlerException");
    }
}
