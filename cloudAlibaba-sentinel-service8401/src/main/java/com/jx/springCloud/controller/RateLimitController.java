package com.jx.springCloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.jx.springCloud.entities.CommonResult;
import com.jx.springCloud.myHandler.CustomerBlockHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author LDW
 * @date 2022/4/14 21:15
 */
@RestController
@Slf4j
public class RateLimitController {

    @GetMapping("/byResource")
    @SentinelResource(value = "byResource", blockHandler = "byResourceHandler")
    public CommonResult byResource1() {
        return new CommonResult(200, "success");
    }

    @SuppressWarnings("all")
    public CommonResult byResourceHandler(BlockException blockException) {
        return new CommonResult(444, "fail");
    }

    @GetMapping("/byResource2")
    @SentinelResource(value = "byResource2")
    public CommonResult byResource2() {
        return new CommonResult(200, "byResource2 success");
    }

    @GetMapping("/rateLimit/customerBlockHandler")
    @SentinelResource(
            value = "customerBlockHandler",
            blockHandlerClass = CustomerBlockHandler.class,
            blockHandler ="handlerException2"
    )
    public CommonResult customerBlockHandler() {
        return new CommonResult(200, "按照url限流");
    }

}
