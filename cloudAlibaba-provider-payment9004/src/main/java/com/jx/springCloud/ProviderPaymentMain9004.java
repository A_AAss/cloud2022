package com.jx.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author LDW
 * @date 2022/4/15 11:53
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ProviderPaymentMain9004 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderPaymentMain9004.class, args);
    }
}
