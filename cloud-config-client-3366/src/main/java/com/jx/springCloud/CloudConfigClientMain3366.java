package com.jx.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author LDW
 * @date 2022/4/9 17:27
 */
@SpringBootApplication
@EnableEurekaClient
public class CloudConfigClientMain3366 {
    public static void main(String[] args) {
        SpringApplication.run(CloudConfigClientMain3366.class, args);
    }
}
