package com.jx.springCloud.service.impl;

import com.jx.springCloud.entities.CommonResult;
import com.jx.springCloud.service.PaymentService;
import org.springframework.stereotype.Component;

/**
 * @author LDW
 * @date 2022/4/15 17:33
 */
@Component("paymentServiceImpl")
public class PaymentServiceImpl implements PaymentService {
    @Override
    public CommonResult paymentSQL(long id) {
        return new CommonResult(444,"openfeign 降级");
    }
}
