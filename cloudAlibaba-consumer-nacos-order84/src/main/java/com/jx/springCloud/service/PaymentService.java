package com.jx.springCloud.service;

import com.jx.springCloud.entities.CommonResult;
import com.jx.springCloud.service.impl.PaymentServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author LDW
 * @date 2022/4/15 17:11
 */
@FeignClient(name = "provider-payment", fallback = PaymentServiceImpl.class)
public interface PaymentService {

    @GetMapping(value = "/paymentSQL/{id}")
    CommonResult paymentSQL(@PathVariable("id") long id);

}
