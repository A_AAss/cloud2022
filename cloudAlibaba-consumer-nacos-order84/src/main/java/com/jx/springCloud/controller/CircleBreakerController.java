package com.jx.springCloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.jx.springCloud.entities.CommonResult;
import com.jx.springCloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author LDW
 * @date 2022/4/15 15:33
 */
@RestController
public class CircleBreakerController {

    @Value("${service-url.nacos-user-service}")
    private String INVOKE_URL;

    private final RestTemplate restTemplate;

    private final PaymentService paymentService;

    public CircleBreakerController(RestTemplate restTemplate, @Qualifier(value = "com.jx.springCloud.service.PaymentService") PaymentService paymentService) {
        this.restTemplate = restTemplate;
        this.paymentService = paymentService;
    }

    //fallback只管方法异常等，blockHandler只管控制台违规
    @GetMapping("/consumer/fallback/{id}")
    @SentinelResource(
            value = "fallback",
            fallback = "handlerFallback",
            blockHandler = "blockHandler",
            exceptionsToIgnore = {IllegalArgumentException.class}
    )
    public CommonResult fallback(@PathVariable long id) {
        CommonResult commonResult = restTemplate.getForObject(INVOKE_URL + "/paymentSQL/" + id, CommonResult.class);
        if (id == 4) {
            throw new IllegalArgumentException("非法参数异常");
        } else if (Objects.requireNonNull(commonResult).getData().get(Long.toString(id)) == null) {
            throw new NullPointerException("null point exception 该id没有对应记录");
        }
        return commonResult;
    }

    @SuppressWarnings("all")
    public CommonResult handlerFallback(@PathVariable long id, Throwable throwable) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("exceptionMessage", throwable.getMessage());
        return new CommonResult(444, "方法异常兜底", hashMap);
    }

    @SuppressWarnings("all")
    public CommonResult blockHandler(@PathVariable long id, BlockException blockException) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("exceptionMessage", blockException.getMessage());
        return new CommonResult(444, "控制台违规兜底", hashMap);
    }

    //openfeign test
    @GetMapping(value = "/paymentSQL/{id}")
    public CommonResult paymentSQL(@PathVariable("id") long id) {
        return paymentService.paymentSQL(id);
    }

}
