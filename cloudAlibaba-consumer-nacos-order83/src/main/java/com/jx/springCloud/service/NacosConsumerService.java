package com.jx.springCloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author LDW
 * @date 2022/4/12 14:56
 */
@FeignClient(name = "nacos-payment-provider")
public interface NacosConsumerService {
    @GetMapping(value = "/payment/nacos/{id}")
    String getPayment(@PathVariable("id") int id);
}
