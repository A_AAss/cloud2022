package com.jx.springCloud.controller;

import com.jx.springCloud.service.NacosConsumerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author LDW
 * @date 2022/4/12 10:59
 */
@RestController
public class NacosConsumerController {
    @Value("${service-url.nacos-user-service}")
    private String INVOKE_URL;

    private final RestTemplate restTemplate;
    private final NacosConsumerService consumerService;

    public NacosConsumerController(RestTemplate restTemplate, NacosConsumerService consumerService) {
        this.restTemplate = restTemplate;
        this.consumerService = consumerService;
    }
    //RestTemplate+Ribbon方式进行服务调用
    @GetMapping("/consumer/payment/nacos/{id}")
    public String getPayment(@PathVariable("id") int id) {
        return restTemplate.getForObject(INVOKE_URL + "/payment/nacos/" + id, String.class);
    }
    //openfeign的方式进行服务调用
    @GetMapping("/consumer/openfeign/payment/nacos/{id}")
    public String getPayment2(@PathVariable("id") int id) {
        return consumerService.getPayment(id);
    }
}
